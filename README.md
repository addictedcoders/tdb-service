[![pipeline status](https://gitlab.com/addictedcoders/test-dashboard-swagger/badges/master/pipeline.svg)](https://gitlab.com/addictedcoders/test-dashboard-swagger/commits/master)

Building a test dashboard with an API generated and documented with Swagger.
